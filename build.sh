#!/bin/bash

if [[ -z "$1" ]]; then
	echo "$0 <version>"
	exit 1
fi
version="$1"
zipfile="tl2rssPL-${version}.zip"
cd ..
if [[ -f "$zipfile" ]]; then
	rm "$zipfile"
fi
zip -r tl2rssPL-${version}.zip tl2rssPL -x "tl2rssPL/.git*" "*/conf.properties" "*/torrent.list" "*/cookies.txt" "*/build.sh"
cp tl2rssPL-${version}.zip /var/www/jkt.im/htdocs/projects/
cp tl2rssPL-${version}.zip /var/www/jkt.im/htdocs/projects/tl2rssPL-latest.zip
