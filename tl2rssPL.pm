package tl2rssPL;

use Data::Dumper;
use Tie::File;

use tl2rssPL::NetIO;
use tl2rssPL::TorrentStore;
use tl2rssPL::Properties;

my $io;
my $ts;

sub new {
	my $class = shift;

	my $self = {};

	bless $self, $class;
	
	$io = tl2rssPL::NetIO->new;
	$ts = tl2rssPL::TorrentStore->new;

	$self->init;
	return $self;
}

sub init {
	my $self = shift;

	$i = 0;
	while(!$self->is_logged_in) {
		if($i) { die "Invalid username/password combination.\n"; }
		$self->login;
		$i++;
	}
}

sub refresh_torrents {
	my $self = shift;

	$ts->read_write;

	my @pages = 1;
	if(!$ts->num_torrents) {
		unshift @pages, 2;
		unshift @pages, 3;
		unshift @pages, 4;
	}
	my @categories = qw(26 32);

	for my $cat(@categories) {
		for my $page (@pages) {
			my $url = "http://www.torrentleech.org/torrents/browse/index/categories/$cat/page/$page";
			my $content = $io->get($url);
			while($content =~ m#<a href="/download/(\d+)/(\S+?.torrent)">#sg) {
				$id = $1;
				$name = $2;
				$ts->add($id, $name)
			}	
		}
	}
}

sub is_logged_in {
	my $self = shift;

	$content = $io->get("http://www.torrentleech.org");
	if($content) {
		if($content !~ m#/user/account/login#) {
			return 1;
		}
	} else {
		die "Unable to check if logged in";
	}
	return 0;
}

sub login {
	my $self = shift;
	
	my $prop = new tl2rssPL::Properties("conf.properties");
	die "unconfigured 'username' in conf.properties" unless $prop->get("username");
	die "unconfigured 'password' in conf.properties" unless $prop->get("password");

	my %keys = (
		username => $prop->get("username"),
		password => $prop->get("password"),
		remember_me => 1,
	);
	
	$io->post("http://www.torrentleech.org/user/account/login/", %keys);
}

sub fetch_torrent {
	my $self = shift;
	my $id = shift;
	my $file = shift;

	$res = $io->get_raw("http://www.torrentleech.org/download/$id/$file");
	return $res;
}

1;
