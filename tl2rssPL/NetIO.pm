package tl2rssPL::NetIO;

use strict;
use warnings;

use LWP::UserAgent;
use HTTP::Cookies;
use HTTP::Request::Common qw(POST GET);

use constant COOKIE_FILE => "cookies.txt";
my $singleton;

sub new {
	my $class = shift;

	return $singleton if defined $singleton;

	$singleton = {};
	bless $singleton, $class;

	my $ua = LWP::UserAgent->new;
	$ua->agent("tl2rssPL");
	$ua->cookie_jar(HTTP::Cookies->new(file => COOKIE_FILE, autosave => 1, ignore_discard => 1));

	if(-f COOKIE_FILE) {
		$ua->cookie_jar->load;
	}

	$singleton->{'ua'} = $ua;
	return $singleton;
}

sub get {
	my($self, $url) = @_;

	if(my $res = $self->get_raw($url)) {
		return $res->content;
	}
	return 0;
}

sub get_raw {
	my($self, $url) = @_;
	my $req = GET $url;
	my $res;
	$res = $self->{'ua'}->request($req);

	return $res;
}

sub post {
	my($self, $url, %param) = @_;

	my $req = POST $url, [ %param ];
	my $res = $self->{'ua'}->request($req);

	return 0 if($res->is_error);

	return $res->content;
}
1;
