package tl2rssPL::TorrentStore;

use constant TORRENT_FILE => "torrent.list";

my $singleton;

my %torrents;

sub new {
	my $class = shift;

	return $singleton if defined $singleton;

	$singleton = {"read_only" => 1};
	bless $singleton, $class;

	$singleton->init;

	return $singleton;	
}

sub read_write {
	$_[0]->{'read_only'} = 0;	
}

sub init {
	my $self = shift;

	if(-f TORRENT_FILE) {
		open(IN, TORRENT_FILE) || die "Unable to open ".TORRENT_FILE.": $!\n";
		while(<IN>) {
			chomp;
			my($id, $time, $name) = split(",", $_, 3);
			$self->add($id, $name, $time);
		}
	}
}

sub add {
	my $self = shift;
	my $id = shift;
	my $name = shift;
	my $time = shift || time;
	if(!exists $torrents{$id}) {
		push @{ $torrents{$id} }, ($name, $time);
	}
}

sub num_torrents {
	return scalar(keys %torrents);
}

sub DESTROY {
	my $self = shift;

	exit if $self->{'read_only'} == 1;

	open(OUT, ">".TORRENT_FILE.".tmp");
	foreach(keys %torrents) {
		print OUT "$_,$torrents{$_}[1],$torrents{$_}[0]\n";
	}
	close(OUT);
	rename TORRENT_FILE.".tmp", TORRENT_FILE;
}

sub get_all {
	return \%torrents;
}

sub getId {
	return $torrents{$_[1]} if exists  $torrents{$_[1]};
	return 0;
}

1;
