package tl2rssPL::Properties;

use strict;
use warnings;

my $singleton;

sub new {
	my $class = shift;
	my $file = shift;

	return $singleton if defined $singleton;

	$singleton = {};
	bless $singleton, $class;

	$singleton->load_properties_file($file);

	return $singleton;
}

sub load_properties_file {
	my($self, $file) = @_;

	die "Unable to open properties file: $file" if(!-f $file);

	open(IN, $file);
	while(<IN>) {
		chomp;
		s/#.*$//;
		next if /^\s*$/;
		next if /^\s*#.*$/;	# allow for comments
		my($key, $value) = split /\s*=\s*/;
		$self->{$key} = $value;
	}
	close(IN);
	die "conf.properties has not been edited!" if $self->get("edited");
}

sub get {
	my($self, $prop, $default) = @_;

	return $self->{$prop} if(exists $self->{$prop});
	return $default if $default;

	return 0;
}

1;
